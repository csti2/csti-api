# Credit card service at AWS Lambda

The project consists of creating a service that allows users to securely store and retrieve credit card information. The service must be implemented using technologies such as Node.js, TypeScript, and AWS Lambda. It should also include unit and integration testing using Jest. In addition, AWS best practices for using Lambdas should be followed and a folder structure should be created following design patterns.

## Requirements

- [Node.js](https://nodejs.org/es) (v14 or later)
- NPM (v7 or later)
- [Serverless](https://www.serverless.com/) Framework (v2 or later)

## Installation

1. Clone the repository:
```
git clone https://gitlab.com/csti2/csti-api.git
```
3. Install Serverless:
```
npm install -g serverless
```
3. Install dependencies:
```
npm install
```

## Configuration

Copy the `env.example` file to `.env` and set the environment variables as needed.

## Run

### Running locally

To run the project locally, execute the following command:

```
npm run dev
```
The application should be available at `http://localhost:3000`.

## Usage

### Create a credit card

To create a credit card, send a `POST` request to `/token/create` with the following data in the body:

```json
{
  "email": "jane@example.com",
  "card_number": "4242424242424242",
  "cvv": "123",
  "expiration_year": "2024",
  "expiration_month": "05"
}
```
The response will be the ID (token) of the card created.

### Obtain card data

To obtain the data of a card, send a `POST` request to `/token/get` with the following body:

```json
{
  "token": "xxxx00yyyy00zzzz"
}
```
Where xxxx00yyyy00zzzz is the ID (token) of the card to be queried.

The response will be a JSON object with the card data.

## Test

Para ejecutar los tests unitarios, ejecutar el siguiente comando:

```
npm run test
```

## Deploy

To deploy the project, execute the following command:

```
npm run deploy
```

## License

This project is licensed under the MIT License.

## Contributors

| [![JJohan][jjohan_avatar]][jjohan_homepage]<br/>[JJohan][jjohan_homepage] |
| ------------------------------------------------------------------------------------------------ |

[jjohan_homepage]: https://gitlab.com/JJohanGL
[jjohan_avatar]: https://secure.gravatar.com/avatar/62640ec554c938fa246d5e29064682d4?s=150
