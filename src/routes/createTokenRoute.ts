import { APIGatewayProxyHandler } from 'aws-lambda'
import { CardController } from '../controllers/cardController';

const cardController = new CardController();

export const handler: APIGatewayProxyHandler = async (event) => {
  try {
    const result = await cardController.createTokenController(event);
    return result;
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      body: JSON.stringify({ message: error }),
    };
  }
};