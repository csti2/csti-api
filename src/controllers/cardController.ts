import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import { decodeToken, encodeToken, generateToken } from '../utils/tokens';
import { validateCard, validateEmail, validateExpirationDate } from '../utils/validations';
import { CardModel } from '../models/cardModel';

export class CardController {
  public async createTokenController(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    try {
      const { email, card_number, cvv, expiration_year, expiration_month } = JSON.parse(event.body!);

      validateEmail(email);
      validateCard(card_number);
      validateExpirationDate(expiration_year, expiration_month);

      const cardToken = encodeToken({ email, card_number, cvv, expiration_year, expiration_month })
      const token = generateToken()
      const card = await CardModel.create({
        id: token,
        cardToken
      })
      return {
        statusCode: 200,
        body: JSON.stringify(card.id),
      };
    } catch (error) {
      return {
        statusCode: 500,
        body: JSON.stringify({ message: "Error storing data" }),
      };
    }
  };
  public async getTokenDataController(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    try {
      const { token } = JSON.parse(event.body!)
      const card = await CardModel.findByPk(token)
      const cardData = decodeToken(card.cardToken)
      return {
        statusCode: 200,
        body: JSON.stringify(cardData),
      }
    } catch (error) {
      return {
        statusCode: 500,
        body: JSON.stringify({ message: "Error finding  data" }),
      }
    }
  };
}