import { APIGatewayProxyHandler, APIGatewayProxyEvent } from 'aws-lambda'
import { handler as createTokenHandler } from './routes/createTokenRoute'
import { handler as getTokenDataHandler } from './routes/getTokenDataRoute';

const routes = {
  POST: {
    '/token/create': createTokenHandler,
    '/token/get': getTokenDataHandler,
  },
};

export const handler: APIGatewayProxyHandler = async (event: APIGatewayProxyEvent) => {
  try {
    const { requestContext } = event;

    const httpMethod = requestContext['http']['method'];
    const path = requestContext['http']['path'];

    if (!routes[httpMethod] || !routes[httpMethod][path]) {
      return {
        statusCode: 404,
        body: JSON.stringify({ message: 'Route not found' }),
      };
    }
    
    const handler = routes[httpMethod][path];
    
    return handler(event);

  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      body: JSON.stringify({ message: error }),
    };
  }
};