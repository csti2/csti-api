export const validateEmail = (email: string): boolean => {
  const regex = /\S+@\S+\.\S+/;
  if (!regex.test(email)) {
    throw new Error('Invalid email');
  }
  return true;
}
export const validateCard = (cardNumber: string): boolean => {
  const regex = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
  if (!regex.test(cardNumber)) {
    throw new Error('Invalid card number');
  }
  return true;
};
export const validateExpirationDate = (expirationYear: string, expirationMonth: string): boolean => {
  const currentYear = new Date().getFullYear();
  const year = parseInt(expirationYear);
  const month = parseInt(expirationMonth);
  if (isNaN(year) || year < currentYear || year > currentYear + 5 || isNaN(month) || month < 1 || month > 12) {
    throw new Error('Invalid expiration date');
  }
  return true;
};