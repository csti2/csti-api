import crypto from 'crypto';
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

dotenv.config();
const { JWT_SECRET_KEY, CRYPTO_KEY, CRYPTO_AVI } = process.env;

interface CardAttributes {
  email: string;
  card_number: string;
  cvv?: string;
  expiration_year: string;
  expiration_month: string;
  jwtToken?: string;
}

export const generateToken = (): string => {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  const randomBytes = crypto.randomBytes(16);
  const charLength = chars.length;

  for (let i = 0; i < 16; i++) {
    let byte = randomBytes[i];
    result += chars[byte % charLength];
  }

  return result;
};

export const encodeToken = (card: CardAttributes): string => {
  const expirationTime = Math.floor(Date.now() / 1000) + (15 * 60);
  const tokenPlain = jwt.sign({ ...card, exp: expirationTime }, `${JWT_SECRET_KEY}`);
  const cipher = crypto.createCipheriv('aes-256-cbc', `${CRYPTO_KEY}`, `${CRYPTO_AVI}`);

  let ciphertext = cipher.update(tokenPlain, 'utf8', 'hex');
  ciphertext += cipher.final('hex');
  return ciphertext;
};

export const decodeToken = (ciphertext: string): CardAttributes => {
  const decipher = crypto.createDecipheriv('aes-256-cbc', `${CRYPTO_KEY}`, `${CRYPTO_AVI}`);
  let token = decipher.update(ciphertext, 'hex', 'utf8');
  token += decipher.final('utf8');
  const decodeToken = jwt.verify(token, `${JWT_SECRET_KEY}`);
  return {
    email: decodeToken['email'],
    card_number: decodeToken['card_number'],
    expiration_year: decodeToken['expiration_year'],
    expiration_month: decodeToken['expiration_month'],
    jwtToken: token
  };
};
  
