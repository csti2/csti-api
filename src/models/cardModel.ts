import { pool } from '../db/postgres';

interface CardModelAttributes {
  id: string;
  cardToken: string;
}

export const CardModel = {
  async create(card: CardModelAttributes): Promise<CardModelAttributes> {
    try {
      /* const client = createClient({
        host: '127.0.0.1',
        port: 6379
      } as RedisClientOptions)
      await client.set(card.id, card.cardToken); */
      await pool.query('INSERT INTO cards (id, card_token) VALUES ($1, $2)', [card.id, card.cardToken]);
      return card;
    } catch(error) {
      // console.log(error)
      throw new Error('Duplicate key value violates unique constraint')
    }
  },

  async findByPk(id: string): Promise<CardModelAttributes> {
    try {
      /* const redisTokenData = await redisClient.get(token);
      if (!redisTokenData) {
        throw new Error('Token not found');
      }
      const tokenData = JSON.parse(redisTokenData) as Token; */
      const { rows } = await pool.query('SELECT * FROM cards WHERE id = $1', [id]);
      if (rows.length === 0) {
        throw new Error('Card does not exist');
      }

      return {
        id: rows[0]['id'],
        cardToken: rows[0]['card_token']
      };
    } catch(error) {
      // console.log(error)
      throw new Error('Card does not exist')
    }
  }
}
