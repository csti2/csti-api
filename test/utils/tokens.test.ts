import { generateToken, encodeToken, decodeToken } from '../../src/utils/tokens';

describe('Token utilities', () => {
  const card = {
    email: 'test@example.com',
    card_number: '1234567890123456',
    expiration_year: '2025',
    expiration_month: '12',
  };

  test('generates a token with 16 characters', () => {
    const token = generateToken();
    expect(token).toHaveLength(16);
  });

  test('encodes and decodes a card object to a token', () => {
    const encodedToken = encodeToken(card);
    const decodedCard = decodeToken(encodedToken);

    expect(decodedCard.email).toBe(card.email);
    expect(decodedCard.card_number).toBe(card.card_number);
    expect(decodedCard.expiration_year).toBe(card.expiration_year);
    expect(decodedCard.expiration_month).toBe(card.expiration_month);
    expect(decodedCard.jwtToken).toBeDefined();
  });
});
