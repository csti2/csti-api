import { validateEmail, validateCard, validateExpirationDate } from '../../src/utils/validations';

describe('Validation utilities', () => {
  describe('validateEmail', () => {
    test('returns true for a valid email', () => {
      expect(validateEmail('test@example.com')).toBe(true);
    });

    test('returns false for an invalid email', () => {
      expect(() => validateEmail('testexample.com')).toThrow('Invalid email');
      expect(() => validateEmail('test@.com')).toThrow('Invalid email');
      expect(() => validateEmail('test@.')).toThrow('Invalid email');
    });
  });

  describe('validateCard', () => {
    test('returns true for a valid card number', () => {
      expect(validateCard('4242424242424242')).toBe(true);
    });

    test('returns false for an invalid card number', () => {
      expect(() => validateCard('123456789012345')).toThrow('Invalid card number');
      expect(() => validateCard('12341234123412341')).toThrow('Invalid card number');
      expect(() => validateCard('abcdabcdabcdabcd')).toThrow('Invalid card number');
    });
  });

  describe('validateExpirationDate', () => {
    test('returns true for a valid expiration date', () => {
      expect(validateExpirationDate('2023', '05')).toBe(true);
    });

    test('throws an error for an invalid expiration date', () => {
      expect(() => validateExpirationDate('2020', '05')).toThrow('Invalid expiration date');
      expect(() => validateExpirationDate('2028', '13')).toThrow('Invalid expiration date');
      expect(() => validateExpirationDate('abcd', 'ef')).toThrow('Invalid expiration date');
    });
  });
});
