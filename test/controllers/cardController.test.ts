import { CardController } from '../../src/controllers/cardController';
import { CardModel } from '../../src/models/cardModel';

const mockCardModelCreate = jest.spyOn(CardModel, 'create');
const mockCardModelFindByPk = jest.spyOn(CardModel, 'findByPk');

describe('CardController', () => {
  const controller = new CardController();

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('createTokenController', () => {
    const event = {
      body: JSON.stringify({
        email: 'example@example.com',
        card_number: '4242424242424242',
        cvv: '123',
        expiration_year: '2024',
        expiration_month: '10',
      }),
      headers: {},
      multiValueHeaders: {},
      httpMethod: "POST",
      isBase64Encoded: false,
      path: "",
      pathParameters: null,
      queryStringParameters: null,
      multiValueQueryStringParameters: null,
      stageVariables: null,
      requestContext: null,
      resource: ""
    };

    test('should return 500 for error storing data', async () => {
      mockCardModelCreate.mockRejectedValue(new Error('error'));
      const result = await controller.createTokenController(event);
      expect(result.statusCode).toBe(500);
      expect(result.body).toBe(JSON.stringify({ message: "Error storing data" }));
      expect(mockCardModelCreate).toHaveBeenCalled();
    });
  });

  describe('getTokenDataController', () => {
    const event = {
      body: JSON.stringify({ token: '123' }),
      headers: {},
      multiValueHeaders: {},
      httpMethod: "POST",
      isBase64Encoded: false,
      path: "",
      pathParameters: null,
      queryStringParameters: null,
      multiValueQueryStringParameters: null,
      stageVariables: null,
      requestContext: null,
      resource: ""
    };

    test('should return 500 for error', async () => {
      mockCardModelFindByPk.mockRejectedValue(new Error('error'));
      const result = await controller.getTokenDataController(event);
      expect(result.statusCode).toBe(500);
      expect(result.body).toBe(JSON.stringify({ message: "Error finding  data" }));
      expect(mockCardModelFindByPk).toHaveBeenCalled();
    });

  });
});