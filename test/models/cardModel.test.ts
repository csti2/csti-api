import { pool } from '../../src/db/postgres';
import { CardModel } from '../../src/models/cardModel';

describe('CardModel', () => {
  const card = {
    id: 'XD5U06YX9mjj7Git',
    cardToken: '92e713ab6e0c8c4779669ecd724d047e459b0b65abc4110729e2de414d3f17ea1fca286b692e7d42d1249fc04acc2fbc3c10ac4b979872ecabf4095bf56f5d0e8e4a973553a9cfe2579190e0de096488dcb143f64ea219d37d224c32a3fa09c8f7955c453c0e797afc9552858928159f05dea714680cf278bd60b60841c7fefff7d401a38cfbf86a58b2e0a3294cc8210bf157ad7bee7fbe9ea8696cbae24311319a77645f2a6d9b1414bfcc392546804aa9353e2e621a1daab9d902e0b4196fc414af71bd6fb45335cc60738c3f246ada303f15537ccd01cd56985df48986d785fb8043d7fbac37b2b77223b2d1558acb020682563414f5d888061e8353e11ef2a1ce87c756cbfe7a788923a43088ce'
  };
  const card2 = {
    id: 'K975HAKk73nn7std',
    cardToken: '92e713ab6e0c8c4779669ecd724d047e459b0b65abc4110729e2de414d3f17ea1fca286b692e7d42d1249fc04acc2fbc3c10ac4b979872ecabf4095bf56f5d0e8e4a973553a9cfe2579190e0de096488dcb143f64ea219d37d224c32a3fa09c8f7955c453c0e797afc9552858928159f05dea714680cf278bd60b60841c7fefff7d401a38cfbf86a58b2e0a3294cc8210bf157ad7bee7fbe9ea8696cbae24311319a77645f2a6d9b1414bfcc392546804aa9353e2e621a1daab9d902e0b4196f332e800feb8466dc35a5f1f0b35ec197f7d890dc97460380f771ae55f80d847e6c29b1eae1e48b675383a21059d5fd0336c9d628715e25cfd832fe71851b084bf99b4a60b0ea98c9a741e7158fdec8af852147a7aa6bcb68442c2f4591ddc90e07aa9c4604b66cf3db4b36c54afbd8aa'
  };

  describe('create', () => {
    beforeEach(async () => {
      // Delete the card before each test to ensure a clean slate
      pool.connect();
      await pool.query('DELETE FROM cards WHERE id = $1', [card.id]);
    });

    it('should create a new card in the database', async () => {
      const createdCard = await CardModel.create(card);
      expect(createdCard).toEqual(card);

      const { rows } = await pool.query('SELECT * FROM cards WHERE id = $1', [card.id]);
      expect(rows.length).toBe(1);
      expect(rows[0]['card_token']).toBe(card.cardToken);
    });

    it('should throw an error if the card already exists', async () => {
      await CardModel.create(card);
      await expect(CardModel.create(card)).rejects.toThrow('Duplicate key value violates unique constraint');
    });
  });

  describe('findByPk', () => {
    beforeEach(async () => {
      pool.connect();
      // Delete the card before each test to ensure a clean slate
      await pool.query('DELETE FROM cards WHERE id = $1', [card.id]);
      // Insert the card before each test to ensure a valid test case
      await CardModel.create(card);
    });

    it('should find a card by primary key', async () => {
      const foundCard = await CardModel.findByPk(card.id);
      expect(foundCard).toEqual(card);
    });

    it('should throw an error if the card does not exist', async () => {
      await expect(CardModel.findByPk('2')).rejects.toThrow('Card does not exist');
    });
  });
});
